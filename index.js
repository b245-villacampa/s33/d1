console.log("REST API!");

// [SECTION] JS SYNCHRONOUS VS ASYNCHRONOUS

	// By default, JS is synchronous meaning it only executes one statement at a time.

console.log("Hello World!");
// conole.log("Hello");

// for(let i=0; i<= 1500; i++){
// 	console.log(i);
// }
console.log("I am the console.log after for loop!");

// async and await - it lets us to proceed or execute more than one statements at the same time.

//[SECTION] Getting all POST 
	// The fetch API allows us to asynchronously request for a resource or data
		// fetch() method in JS is used to request to the server and load information on the webpage
	// Syntax:
		// fetch("apiURL");

	console.log(fetch("https://jsonplaceholder.typicode.com/posts"));
	/*
		a "promise" is an object that represent the eventual completion (or failure) of an asynchronous function and it's resulting value.
	*/

		// A promise may be in one of the 3 possible states: fulfilled, rejected and pending.
		/*
			pending: initial state, neither fulfilled nor rejected response
			fulfilled: operation was completed
			rejected: operation rejected 
		*/

		//Syntax
	// fetch("apiURL").then(response => {what will you do with the response})

	fetch("https://jsonplaceholder.typicode.com/posts").then(response => response.json()).then(data => console.log(data));
	// The ".then()" captures the response object and return another promise will be either resolved or rejected

	fetch("https://jsonplaceholder.typicode.com/posts").then(response => response.json()).then(data => {
		let title = data.map(element => element.title);
		console.log(title);
	});


	// The "async" and "awake" keyword to achieve asynchronous code

	async function fetchData(){
		let result = await(fetch("https://jsonplaceholder.typicode.com/posts"))
		console.log(result);
		let json = await result.json();
		console.log(json);
	}

	// Since it is method, fetchDate should be provoke to run.
	fetchData();

	// [SECTION] Get A specific post
		// Retrieves a specific post following the REST API(/post/:id)
	// wildcard is where you can put any value, it then creates a link between id parameter in the URL and the value provided in the URL

	fetch("https://jsonplaceholder.typicode.com/posts/3", {method: "GET"}).then(response => response.json()).then(result => console.log(result));

	// [SYNTAX] Creating POST
	/*
		Syntax:
			fetch("apiUrl", {necessaryObject}).then(response => response.json()).then(result => {codeblock});
	*/


	fetch("https://jsonplaceholder.typicode.com/posts", {
		// REST API method to execite
		method: "POST",
		// headers, it tells us the data type of our request
		headers:{
			"Content-Type":"application/json"
		},
		// body- contains the fata that will be aded to the database/the request of the user
		body :JSON.stringify({
			title:"New Post",
			body:"Hello World",
			userId:1
		})
	}).then(response => response.json()).then(result => console.log(result));


	// [SECTION] Updating a post
	// PUT - whole 

	fetch("https://jsonplaceholder.typicode.com/posts/5", {
		method:"PUT",
		headers:{
			"Content-Type" : "application/json"
		},
		body: JSON.stringify({
			title:"updated post",
			body:"Hello Again",
			userId:1
		})		
	}).then(response => response.json()).then(result => console.log(result));

// [Section] Patch
	// patch - properties
	fetch("https://jsonplaceholder.typicode.com/posts/1", {
		method:"PATCH",
		headers:{
			"Content-Type" : "application/json"
		},
		body: JSON.stringify({
			title:"updated title!",
			
		})		
	}).then(response => response.json()).then(result => console.log(result));


	// Mini activity
		// change the body property of the documnet which has the id of 18 to "Hello it's me!"


	fetch("https://jsonplaceholder.typicode.com/posts/18", {
		method:"PATCH",
		headers:{
			"Content-Type":"application/json"
		},
		body: JSON.stringify({
			body:"Hello it's me!"
		})
	}).then(response => response.json()).then(result => console.log(result));

	// [SECTION] - DELETE A POST

		fetch("https://jsonplaceholder.typicode.com/posts/5", {method:"DELETE"}).then(response => response.json()).then(result => console.log(result));